# redis-stat

redis-stat是一个使用Ruby写的Redis监控工具

它是基于[Redis命令行](http://redis.io/commands/info)的而不像其它的监控工具是基于[监控器](http://redis.io/commands/monitor)的,因此它捕获影响到Redis实例的性能.

_redis-stat_ 允许你监控Redis实例
- 可以使用vmstat-like在终端输出
- 或者使用嵌入的web服务器在页面上输出

## 安装

```
gem install redis-stat
```

如果你有任何关于Ruby环境配置的问题，你可以[直接下载Jar文件](https://github.com/junegunn/redis-stat/releases)并直接使用

## 使用

```
usage: redis-stat [HOST[:PORT][/PASS] ...] [INTERVAL [COUNT]]

    -a, --auth=PASSWORD              密码
    -v, --verbose                    显示更多的信息
        --style=STYLE                输出风格: unicode|ascii
        --no-color                   ANSI代码颜色
        --csv=OUTPUT_CSV_FILE_PATH   保存结果到CSV文件中
        --es=ELASTICSEARCH_URL       发送结果到搜索引擎解决方案中: [http://]HOST[:PORT][/INDEX]

        --server[=PORT]              启动redis-stat web服务器 (默认端口: 63790)
        --daemon                     Daemonize redis-stat. Must be used with --server option.

        --version                    显示版本
        --help                       显示帮助信息
```

## 从命令行运行redis-stat监控器

```
redis-stat
redis-stat 1
redis-stat 1 10
redis-stat --verbose
redis-stat localhost:6380 1 10
redis-stat localhost localhost:6380 localhost:6381 5
redis-stat localhost localhost:6380 1 10 --csv=/tmp/output.csv --verbose
```

### 屏幕截屏

![终端输出](https://github.com/scalad/redis-stat/blob/master/screenshots/redis-stat-0.3.0.png)

## 在web浏览器中的redis-stat

当设置了`--server`选项，redis-stat将会打开一个内嵌的web服务器（默认的端口是63790）

这样子你就可以使用浏览器来监控Rddis了.

```
redis-stat --server
redis-stat --verbose --server=8080 5

# redis-stat server can be daemonized
redis-stat --server --daemon

# Kill the daemon
killall -9 redis-stat-daemon
```
## 支持Windows

如果你是在Windows上运行，你只能在[JRuby](http://jruby.org)上安装redis-stat，因为是在默认的Windows命令提示符上，所以请注意在Windows终端是不会支持有颜色的字符的。

## 贡献

1. 复制项目
2. 创建你的特色的分支 (`git checkout -b my-new-feature`)
3. 提交你的更改 (`git commit -am 'Added some feature'`)
4. 推送到分支 (`git push origin my-new-feature`)
5. 创建新的请求

### 测试

你需要两个端口分别为6379和6378的没有密码的Redis本地的服务器

```sh
bundle install
bundle exec rake test
```
